#include<stdio.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include <stdlib.h>
#include<string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(){
  /* "sockaddr_in" estructura con inf. de este host
  que usará este socket para este proceso server
  */    
  struct sockaddr_in atributoserv;
  //Protocolo para este socket de este server
  atributoserv.sin_family=AF_INET;  //Familia IPV4
  //Dirección IP para el socket de este server 
  
  // atributoserv.sin_addr.s_addr=inet_addr("172.17.0.2");
  atributoserv.sin_addr.s_addr=INADDR_ANY;//cualquier interface (lo,0/0 o wifi)
  // Puerto TCP para este el socket de este server 
  atributoserv.sin_port=htons(2222); 

  int sockpass=socket(AF_INET,SOCK_STREAM,0);//SOCK_STREAM es TCP flujo
  // Seteo a TCP/IP que me deje reutilizar ip y puerto. "desconexión de FIN de circuito virtual"
  int activado = 1; //SO_REUSEADDR indica a S.O reutilizar atributos de sockPasivo 
  setsockopt(sockpass,SOL_SOCKET, SO_REUSEADDR, &activado,sizeof(activado));
  //(void *) pq pide struct sockaddr  y estoy posando una específica sockaddr_in atributoserv 
  // tambien (struct sockaddr *)&atributoserv
  if( bind(sockpass,(struct sockaddr *)&atributoserv,sizeof(atributoserv)) < 0){
    //Valido bidn x si otro proceso escucha este mismo puesto. FIN demora 2 RTT "de tele"
    perror("falló bind");
    return -1;
  }
  //SOMAXCONN, so establece tamaño optimo/max de la cola
  listen(sockpass, 100);

  struct sockaddr_in atritutosclie;
  unsigned int clilong;
while(1){
  //int sockactvo =accept(sockpass,(void *)&atritutosclie, &clilong);
  int sockactvo =accept(sockpass,(struct sockaddr *)&atritutosclie, &clilong);
     
  int leids=0;
  char mensa[1000];  memset(mensa, '\0', sizeof(mensa));
  char cabecera[4024];  memset(cabecera, '\0', sizeof(cabecera));
  /*******char prueba[]="HTTP/1.1 200 OK\n\
      Content-Type: text/html; charset=UTF-8\n\
      \n\n <html><body><h1>Página principal de tuHost</h1>\
  (Contenido)  .  .  .</body></html>\n";
  *******/ 
 /*******char prueba[]="HTTP/1.1 200 OK\n\
      Content-Type: text/html; charset=UTF-8\n\
      \n\n { hol:\"joo\"; jyy:\"ppd\"}\n";*******/

  char *rest=NULL, *token=NULL, *retorno[50]; int i=0;
  
  //while(1){
    /*****LEO DEL CLIENTE EN READ(sockactvo Y GESTIONO LO QUE ME PIDE, ENTREGO EN EL WRITE(sockactvo)*****/  
    while((leids=read(sockactvo,mensa,sizeof(mensa))) > 0){ //mensa tiene el GET /recurso
       // mensa almacena GET /recurso.ext HTTP/1.1 miUrl-HOST .... etc 
      /************ LEO DESDE CUALQUIER CLIENTE web o clasero***************/
      write(1,mensa,leids); // mensa almacena cabecera GET de cliente con recurso y nombreTracker
        // BUSCAR -> recurso.ext.semilla, ABRIR Y ENTREGAR A CLIENTE NUEVAMENTE
        /******printf(" xxx %s\n",prueba);
        write(sockactvo,prueba,strlen(prueba));******/
      /* ACÁ INTERPRETO MI CUASI-PROTOCOLO EN FORMATO CUASI-JSON NO ES H*/
      char *ptr = mensa; 
      //parcéo GET /recurso.ext HTTP/1.1 miUrl-HOST.... etc
      while( (token = strtok_r(ptr, " \n/:", &rest))){ 
        retorno[i]=token;//printf(" i=%d, tock=%s y tamaño=%ld ->\n",i, retorno[i],strlen(retorno[i]));
        ptr = rest; //printf("i = %d, tock -> %s\n",i,retorno[i]);  
        i++;  
      }

      /*Separando nombre de extención Diir=nombre.ext*/
      char temp[50]; memset(temp, '\0', sizeof(temp));
      char diir [50]; memset(diir, '\0', sizeof(diir));
      char extencion[5]; memset(extencion, '\0', sizeof(extencion));
      char * pch;  
      
      /*** NOMBRE DE RECUROS = NOMBRE DE DIR, Lo separaro de ext pa buscarlo y armar path ***/
      strncpy(temp,retorno[1],strlen(retorno[1]));
      
      /*parcing en pch solo Diir=nombre, sin .ext*/
      pch=strchr(temp,'.');
      
      /*almaceno solo nombre en diir=nombre */
      strncpy(diir,temp,(strlen(temp) -strlen(pch)));
      /*me corro un char (+1) y almaceno solo extencion sin punto "." en extencion=ext */    
      strncpy(extencion,(pch+1),(strlen(pch+1))); //printf( "nombre=%s y extencion=%s y recurso %s\n", diir, extencion,retorno[1] );

      /*formando la ruta de recurso pedido por cliente en retorno[1]*/
      char ruta_trrn[512]; memset(ruta_trrn, '\0', sizeof(ruta_trrn));
      sprintf(ruta_trrn,"../util_bt/%s/%s.semilla",diir,retorno[1]);//printf("ruta %s\n",ruta_trrn); 
      //sprintf(ruta_trrn,"../util_bt/%s.semilla",retorno[1]);//printf("ruta %s\n",ruta_trrn); 
      
      /*Abriendo recurso retorno[1] */
      int fd=0;
      if( (fd=open(ruta_trrn,O_RDONLY,O_APPEND,S_IRUSR)) < 0)
        return -1;

      /*obtención de peso de retorno[1] */
      struct stat fileStat;  
      if(fstat(fd,&fileStat) < 0)    
        return -1; //printf("File Size: \t\t%ld bytes\n",fileStat.st_size);

      char recurso[5024]; memset(recurso, '\0', sizeof(recurso));
      int lee_bt =0;
     
     /*******write(sockactvo,prueba,strlen(prueba));*******/
      lee_bt=read(fd,recurso,sizeof(recurso));
      close(fd);
      // HTTP requests y responses
      sprintf(cabecera,"\
      HTTP/1.1 200 OK\n\
      Content-Type: application/json\n\
      Content-Length: %ld\n\n%s",fileStat.st_size,recurso);// printf("Cabecera response%s:\n",cabecera);

      write(sockactvo,cabecera,strlen(cabecera));   
      write(1,cabecera,strlen(cabecera));   


      /* while( (lee_bt=read(fd,recurso,sizeof(recurso))) >0 ){
        write(sockactvo,recurso,lee_bt);
      } */   
      close(sockactvo);
    } 
    /* recibiendo de cliente: Enter de envío desde cliente, cuenta como un char más.
    // Para este ejemplo, nc envía stream y no string de caracteres, por eso lo cierro a ese strem con '\0'
    int byteRecibidos=recv(sockactvo,buff,4,0);
    buff[byteRecibidos] = '\0';
    /* dent@OaSYS  ~  nc 127.0.0.1 8080 */
  }
  close(sockpass);
  return 0;
}

/*  RECIBIDO DESDE CLIENTE
    GET /El_Hobbit.txt HTTP/1.1
    Host: localhost
    Accept-Language: es-ES,es;q=0.9
    User-Agent: Mozilla/5.0 (X11; Linux x86_64)

 */