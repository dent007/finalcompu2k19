#ifndef TRACKER_H
#define TRACKER_H

#include<json-c/json.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/fcntl.h>
#include<arpa/inet.h>
#include<sys/socket.h>	
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/mman.h>
#include<pthread.h>

pthread_mutex_t mutex;
//pthread_mutex_t mutex2;
pthread_t *hl;
 struct part{
	char peer_ip[50];
	char trozo[40];
    int port;
} ;
typedef struct {
    char parte[5];
    char tamanio[10];
    char buffer[10240];
} hilo;

int flag;

int h; // GLOBAL PARA COMPARAR HILOS

void *extraer_parte(void *struk);
int  leerFiletorrent(char *torrent);
void logger(char *msgError);
char * buscarTrackerRecurso(int fdTorrent);
int conectarSocket_Http( const char *tracker,const char * recurso,int taman);
void integrar_partes(char *parte,char *tamanio,char *buffer,int fdUnir,int n_segmentos);
int largo_cadena(char *cadena);


#endif