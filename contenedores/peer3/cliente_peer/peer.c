#include<stdio.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include <stdlib.h>
#include<string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(){
   
  struct sockaddr_in atributoserv;
  //Protocolo para este socket de este server
  atributoserv.sin_family=AF_INET;  //Familia IPV4
  //Dirección IP para el socket de este server 
   atributoserv.sin_addr.s_addr=inet_addr("10.10.0.2");
   //atributoserv.sin_addr.s_addr=inet_addr("127.0.0.2");
  //atributoserv.sin_addr.s_addr=INADDR_ANY;//cualquier interface (lo,0/0 o wifi)
  // Puerto TCP para este el socket de este server 
  atributoserv.sin_port=htons(5553); 

  int sockpass=socket(AF_INET,SOCK_STREAM,0);//SOCK_STREAM es TCP flujo
  // Seteo a TCP/IP que me deje reutilizar ip y puerto. "desconexión de FIN de circuito virtual"
  int activado = 1; //SO_REUSEADDR indica a S.O reutilizar atributos de sockPasivo 
  setsockopt(sockpass,SOL_SOCKET, SO_REUSEADDR, &activado,sizeof(activado));
  //(void *) pq pide struct sockaddr  y estoy posando una específica sockaddr_in atributoserv 
  // tambien (struct sockaddr *)&atributoserv
  if( bind(sockpass,(struct sockaddr *)&atributoserv,sizeof(atributoserv)) < 0){
    //Valido bidn x si otro proceso escucha este mismo puesto. FIN demora 2 RTT "de tele"
    perror("falló bind");
    return -1;
  }
  //SOMAXCONN, so establece tamaño optimo/max de la cola
  listen(sockpass,100);

  struct sockaddr_in atritutosclie;
  unsigned int clilong;
while(1){
  //int sockactvo =accept(sockpass,(void *)&atritutosclie, &clilong);
  int sockactvo =accept(sockpass,(struct sockaddr *)&atritutosclie, &clilong);

  int leids=0;
  char mensa[1024];  memset(mensa, '\0', sizeof(mensa));
  char cabecera[1024];  memset(cabecera, '\0', sizeof(cabecera));
  /*******char prueba[]="HTTP/1.1 200 OK\n\
      Content-Type: text/html; charset=UTF-8\n\
      \n\n <html><body><h1>Página principal de tuHost</h1>\
  (Contenido)  .  .  .</body></html>\n";
  *******/ 
  char *rest=NULL, *token=NULL, *retorno[50]; int i=0;
    while((leids=read(sockactvo,mensa,sizeof(mensa))) > 0){ //mensa tiene el GET /recurso
      write(1,mensa,leids); // mensa almacena cabecera GET de cliente con recurso y nombreTracker   
      char *ptr = mensa; 
      //parcéo GET /el_hobbit.04 HTTP/1.1 etc
      while( (token = strtok_r(ptr, " /.", &rest))){ 
        retorno[i]=token;//printf(" i=%d, tock=%s y tamaño=%ld ->\n",i, retorno[i],strlen(retorno[i]));
        ptr = rest; //printf("i = %d, tock -> %s\n",i,retorno[i]);  
        i++;   //retorno[1]= el_hobbit  ,    retorno[2]=04
      }
      //printf("nombre_pedido%s\n",retorno[1]);
      //printf("parte %s\n",retorno[2]);

    /*  TEST VER SALIDA Y FORMATO DE DATOS DE FILE ORIGINAL
      write(1,"\n#####________----________#####\n",32);
      int fd2=open("b_luz.jpg",O_RDONLY,O_APPEND,S_IRUSR);
      int lei; char tem[1024];memset(tem,'\0',sizeof(tem));      
      while((lei=read(fd2,tem,sizeof(tem))) >0){
      write(1,tem,lei);
      }FIN TEST*/


      char ruta_trrn[1024]; memset(ruta_trrn, '\0', sizeof(ruta_trrn));
                                // directorio,nombre,extncion: "luz/luz.00"
      sprintf(ruta_trrn,"/home/cliente_peer/%s/%s.%s",retorno[1],retorno[1],retorno[2]);//printf("ruta %s\n",ruta_trrn); 
      //printf("ruta pedazo -> %s\n",ruta_trrn);
      //write(1,ruta_trrn,strlen(ruta_trrn));
      struct stat fileStat;  int tipo_error;
      int fd=0; 
      //char *tipo_error=

      
      if( (fd=open(ruta_trrn,O_RDONLY,O_APPEND,S_IRUSR)) < 0){
        perror("error en ->open(parte)");
        tipo_error=401;
       //return -1;
      }else{
        tipo_error=200;
        if(fstat(fd,&fileStat) < 0)  {
          tipo_error=401;
        }
      /* TEST VER SALIDA Y FORMATO DE DATOS DE PEDACITOS!!!  
        write(1,"\n>>>>>________----________<<<<<\n",32);
        int lei; char tem[1024];memset(tem,'\0',sizeof(tem));      
        while((lei=read(fd,tem,sizeof(tem))) >0){
          write(1,tem,lei);
        }FIN TEST*/
      }

      char recurso[2024]; memset(recurso, '\0', strlen(recurso));
      int lee_bt =0;

      //lee_bt=read(fd,recurso,sizeof(recurso));
      //close(fd);
      sprintf(cabecera,"HTTP/1.1 %d OK\nContent-Type:txt/html\nContent-Part:%s\nContent-Length:%ld\n\n",tipo_error,retorno[2],fileStat.st_size);// printf("Cabecera response%s:\n",cabecera);
       write(sockactvo,cabecera,strlen(cabecera)); 
       //write(sockactvo,ruta_trrn,strlen(ruta_trrn)); 
      ////TEST char file_test[5024]; memset(file_test, '\0', sizeof(file_test));
      ////TEST sprintf(file_test,"test/%s.%s",retorno[1],retorno[2]);//printf("ruta %s\n",ruta_trrn); 
      ////TEST printf("%s\n",file_test);
      ////TEST int fdU=open(file_test,O_CREAT|O_RDWR,S_IROTH|S_IWOTH|S_IRGRP|S_IWGRP|S_IRUSR|S_IWUSR);//O_APPEND -> write a conti
       while((lee_bt=read(fd,recurso,sizeof(recurso))) > 0){      
         write(sockactvo,recurso,lee_bt);
       ////TEST  write(fdU,recurso,lee_bt);
      }
     close(fd);
    close(sockactvo);
    }     
  }
  close(sockpass);
  return 0;
}

/*  RECIBIDO DESDE CLIENTE
    GET /el_hobbit.04 HTTP/1.1
    Host: 127.0.0.2
    Accept-Language: es-ES,es;q=0.9
    User-Agent: Mozilla/5.0 (X11; Linux x86_64)

 */

 
