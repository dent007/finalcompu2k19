CC = gcc 
CFLAGS = -Wall -g -I/usr/include/json-c/

DIR_T = tracker_bt/
SOURCE_T = $(DIR_T)tracker_buscar.c 

DIR_U = util_bt/
SOURCE_U = $(DIR_U)util_logger.c

DIR_J = juntar_bt/
SOURCE_J = $(DIR_J)juntar.c

#DIR_S = swarm_ut/
#SOURCE_S = $(DIR_S)

#DIR_R = resource_ut
#SOURCE_R =$(DIR_R)

compile: bitTorreum.c $(SOURCE_T) $(SOURCE_U) $(SOURCE_J) 
	$(CC) $(CFLAGS)  -o compile bitTorreum.c  $(SOURCE_T) $(SOURCE_U) $(SOURCE_J) -ljson-c -l pthread
   #(CC) $(CFLAGS)  -o compile bitTorreum.c $(SOURCE_T) $(SOURCE_U) 
clean:
	@rm -rf *.o $(DIR_T)*.o

clear:
	@rm compile

#final:	
#	@./compile -f util_bt/video.torrent

video:	
	@./compile -f util_bt/video.torrent

#imagen:	
#	@./compile -f util_bt/luz.torrent	
#https://gist.github.com/alan-mushi/19546a0e2c6bd4e059fd